package com.khamars.login;

import com.khamars.login.persistence.setup.MyApplicationContextInitializer;
import com.khamars.login.spring.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(exclude = { // @formatter:off
		ErrorMvcAutoConfiguration.class
})// @formatter:on
public class LoginApplication extends SpringBootServletInitializer {

	private final static Class[] CONFIGS = { // @formatter:off
			MyContextConfig.class,
			MyPersistenceJpaConfig.class,
			MyServiceConfig.class,
			MyServletConfig.class,
			MyWebConfig.class,
			LoginApplication.class,

			AuthorizationServerConfiguration.class
	}; // // @formatter:on

	//

	@Override
	protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
		return application.sources(CONFIGS).initializers(new MyApplicationContextInitializer());
	}

	public static void main(final String... args) {
		final SpringApplication springApplication = new SpringApplication(CONFIGS);
		springApplication.addInitializers(new MyApplicationContextInitializer());
		springApplication.run(args);
	}
}
