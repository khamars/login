package com.khamars.login.persistence.dao;

import com.khamars.login.persistence.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IUserJpaDao extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    //
    User findByName(final String name);
    User findByEmail(final String email);
}
