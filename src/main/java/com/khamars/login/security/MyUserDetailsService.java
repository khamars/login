package com.khamars.login.security;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import com.khamars.login.persistence.model.Privilege;
import com.khamars.login.persistence.model.Role;
import com.khamars.login.persistence.model.User;
import com.khamars.login.service.IUserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Component
public class MyUserDetailsService implements UserDetailsService {

    private IUserService userService;

    public MyUserDetailsService() {
        super();
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

        Preconditions.checkNotNull(username);
        final User user = userService.findByEmail(username);

        if (user == null) {
            throw new UsernameNotFoundException("Username was not found: " + username);
        }

        final Set<Role>  rolesOfUser = user.getRoles();
        final Set<Privilege> privilegesOfUser = Sets.newHashSet();

        for(final Role role: rolesOfUser){
            privilegesOfUser.addAll(role.getPrivileges());
        }

        final Function<Object, String> toStringFunction = Functions.toStringFunction();
        final Collection<String> rolesToString = Collections2.transform(privilegesOfUser, toStringFunction);
        final String[] rolesStringAsArray = rolesToString.toArray(new String[rolesToString.size()]);
        final List<GrantedAuthority> auths = AuthorityUtils.createAuthorityList(rolesStringAsArray);

        return new MyUser(user.getEmail(), user.getPassword(), auths, user.getId());

    }
}
