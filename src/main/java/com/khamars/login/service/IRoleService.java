package com.khamars.login.service;


import com.khamars.login.interfaces.IService;
import com.khamars.login.persistence.model.Role;

public interface IRoleService extends IService<Role> {
    Role findByName(String name);
}
