package com.khamars.login.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.khamars.login.persistence.dao.IUserJpaDao;
import com.khamars.login.exception.MyEntityNotFoundException;
import com.khamars.login.persistence.model.User;
import com.khamars.login.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements IUserService{

    @Autowired
    IUserJpaDao dao;

    public UserServiceImpl() {
        super();
    }


    @Override
    @Transactional(readOnly = true)
    public User findOne(long id) {
//        return dao.findById(id).get();
        return dao.getOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return Lists.newArrayList(dao.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAllSorted(String sortBy, String sortOrder) {
        final Sort sortInfo = constructSort(sortBy, sortOrder);
        return Lists.newArrayList(dao.findAll(sortInfo));
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAllPaginated(int page, int size) {
        final List<User> content = dao.findAll(new PageRequest(page, size, null)).getContent();
        if (content == null)
            return Lists.newArrayList();
        return content;
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAllPaginatedAndSorted(int page, int size, String sortBy, String sortOrder) {
        final Sort sortInfo = constructSort(sortBy, sortOrder);
        final List<User> content = dao.findAll(new PageRequest(page, size, sortInfo)).getContent();
        if (content == null)
            return Lists.newArrayList();
        return content;

    }

    @Override
    public User create(User resource) {
        Preconditions.checkNotNull(resource);

        final User persistedEntity = dao.save(resource);

        return persistedEntity;
    }

    @Override
    public void update(User entity) {
        Preconditions.checkNotNull(entity);

        dao.save(entity);
    }

    @Override
    public void delete(long id) {
        final User entity = dao.getOne(id);
        if (entity == null) {
            throw new MyEntityNotFoundException();
        }
        dao.delete(entity);

    }

    @Override
    public void deleteAll() {
        dao.deleteAll();
    }

    @Override
    public long count() {
        return dao.count();
    }

    @Override
    @Transactional(readOnly = true)
    public User findByName(String name) {
        return dao.findByName(name);
    }

    @Override
    public User findByEmail(String email) {
        return dao.findByEmail(email);
    }


    protected final Sort constructSort(final String sortBy, final String sortOrder){
        Sort sortInfo = null;
        if(sortBy != null){
            sortInfo = new Sort(Sort.Direction.fromString(sortOrder), sortBy);
        }
        return sortInfo;
    }

}
