package com.khamars.login.service.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.khamars.login.persistence.dao.IRoleJpaDao;
import com.khamars.login.exception.MyEntityNotFoundException;
import com.khamars.login.persistence.model.Role;
import com.khamars.login.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoleServiceImpl  implements IRoleService {

    @Autowired
    IRoleJpaDao dao;

    public RoleServiceImpl() {
        super();
    }

    // API


    @Override
    @Transactional(readOnly = true)
    public Role findOne(long id) {
//        return dao.findById(id).get();
        return dao.getOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Role> findAll() {
        return Lists.newArrayList(dao.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Role> findAllSorted(String sortBy, String sortOrder) {
        final Sort sortInfo = constructSort(sortBy, sortOrder);
        return Lists.newArrayList(dao.findAll(sortInfo));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Role> findAllPaginated(int page, int size) {
        final List<Role> content = dao.findAll(new PageRequest(page, size, null)).getContent();
        if (content == null)
            return Lists.newArrayList();
        return content;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Role> findAllPaginatedAndSorted(int page, int size, String sortBy, String sortOrder) {
        final Sort sortInfo = constructSort(sortBy, sortOrder);
        final List<Role> content = dao.findAll(new PageRequest(page, size, sortInfo)).getContent();
        if (content == null)
            return Lists.newArrayList();
        return content;

    }

    @Override
    public Role create(Role resource) {
        Preconditions.checkNotNull(resource);

        final Role persistedEntity = dao.save(resource);

        return persistedEntity;
    }

    @Override
    public void update(Role entity) {
        Preconditions.checkNotNull(entity);

        dao.save(entity);
    }

    @Override
    public void delete(long id) {
        final Role entity = dao.getOne(id);
        if (entity == null) {
            throw new MyEntityNotFoundException();
        }
        dao.delete(entity);

    }

    @Override
    public void deleteAll() {
        dao.deleteAll();
    }

    @Override
    public long count() {
        return dao.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Role findByName(String name) {
        return dao.findByName(name);
    }


    protected final Sort constructSort(final String sortBy, final String sortOrder){
        Sort sortInfo = null;
        if(sortBy != null){
            sortInfo = new Sort(Sort.Direction.fromString(sortOrder), sortBy);
        }
        return sortInfo;
    }

}
