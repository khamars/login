package com.khamars.login.service;


import com.khamars.login.interfaces.IService;
import com.khamars.login.persistence.model.Privilege;

public interface IPrivilegeService extends IService<Privilege> {
    Privilege findByName(String name);
    //
}
