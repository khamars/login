package com.khamars.login.service;


import com.khamars.login.interfaces.IService;
import com.khamars.login.persistence.model.User;

public interface IUserService extends IService<User> {
    //
    User findByName(final String name);
    User findByEmail(final String email);
}
