package com.khamars.login.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.khamars.login.service" })
public class MyServiceConfig {

    public MyServiceConfig() {
        super();
    }

    // beans

}
