package com.khamars.login.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan({ "com.khamars.login.web" })
@EnableWebMvc
public class MyWebConfig extends WebMvcConfigurerAdapter {

    public MyWebConfig() {
        super();
    }

    // beans

}
